
var app = angular.module('Ejemplo4',[])

app.controller('GameController', function() {

    this.currentCard = 0;
    this.foto = 'foto1.jpg';
    this.cards = [
        { name: 'Card1', imageSrc: 'foto1.jpg',hidden:false},
        { name: 'Card2', imageSrc: 'foto2.jpg',hidden:true},
        { name: 'Card3', imageSrc: 'foto3.jpg',hidden:false}
    ];

    this.changeCard = function (){

        this.currentCard = (this.currentCard+1)% this.cards.length;
    };

    this.getCurrentCardImage = function()
    {

        return this.cards[this.currentCard].imageSrc;
    };

    this.getCurrentCard = function()
    {

        return this.cards[this.currentCard];
    };

})